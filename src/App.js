import React, { Component, Fragment } from "react";
import Layout from "./components/Layout/Layout";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";

const theme = createMuiTheme({
  typography: {
    useNextVariants: true
  },
  palette: {
    primary: {
      light: "#66BB6A",
      main: "#388E3C",
      dark: "#1B5E20",
      contrastText: "#fff"
    },
    secondary: {
      light: "#F44336",
      main: "#E53935",
      dark: "#D32F2F",
      contrastText: "#fff"
    },
    type: "dark"
  }
});
class App extends Component {
  render() {
    return (
      <MuiThemeProvider theme={theme}>
        <Fragment>
          {" "}
          <Layout />
        </Fragment>
      </MuiThemeProvider>
    );
  }
}

export default App;
