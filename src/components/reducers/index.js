import { combineReducers } from "redux";

import login from "../appBarItems/store/reducers/login";
import signUp from "../appBarItems/store/reducers/signUp";

export default combineReducers({
  login,
  signUp
});
