import React from "react";

const facebookSVG = ({
  height = "40",
  width = "40",
  viewBox = "0 0 512 512",
  className = "",
  pathStyle = { fill: "#ffffff" },
  style = {
    fill: "#3b5998",
    fillOpacity: "1",
    fillRule: "nonzero",
    stroke: "none"
  }
}) => {
  return (
    <svg
      height={height}
      width={width}
      viewBox={viewBox}
      className={className}
      id="Layer_1"
      version="1.1"
      preserveAspectRatio="none"
      // xml:space="preserve"
      xmlns="http://www.w3.org/2000/svg"
      xmlnscc="http://creativecommons.org/ns#"
      xmlnsdc="http://purl.org/dc/elements/1.1/"
      xmlnsinkscape="http://www.inkscape.org/namespaces/inkscape"
      xmlnsfdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
      xmlnssodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
      xmlnssvg="http://www.w3.org/2000/svg"
    >
      <defs id="defs12" />
      <g id="g5991">
        <rect
          height="512"
          id="rect2987"
          rx="64"
          ry="64"
          style={style}
          width="512"
          x="0"
          y="0"
        />
        <path
          d="M 286.96783,455.99972 
                    V 273.53753 
                    h 61.244 l 9.1699,-71.10266 
                    h -70.41246 
                    v -45.39493 
                    c 0,-20.58828 5.72066,-34.61942 35.23496,-34.61942 l 37.6554,-0.0112 
                    V 58.807915 
                    c -6.5097,-0.87381 -28.8571,-2.80794 -54.8675,-2.80794 -54.28803,0 -91.44995,33.14585 -91.44995,93.998125 
                    v 52.43708 
                    h -61.40181 
                    v 71.10266 
                    h 61.40039 
                    v 182.46219 
                    h 73.42707 
                    z"
          id="f_1_"
          style={pathStyle}
        />
      </g>
    </svg>
  );
};

export default facebookSVG;
