import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import stylesLoginWithFacebook from "./styleLoginWithFacebook";
import FacebookSVG from "./facebookIcon";
import FacebookLogin from "react-facebook-login/dist/facebook-login-render-props";

class LoginWithFacebook extends Component {
  state = {
    isLoggedIn: false,
    userID: "",
    name: "",
    email: "",
    picture: ""
  };
  componentClicked = () => {
    console.log("clicked");
  };
  responseFacebook = response => {
    // console.log(response);
    if (response.userID) {
      this.setState({
        isLoggedIn: true,
        userID: response.userID,
        name: response.name,
        email: response.email,
        picture: response.picture.data.url
      });
    }
  };
  shouldComponentUpdate(nextProps, nextState) {
    if (nextState.userID) return true;
    else return false;
  }

  render() {
    const { classes } = this.props;
    let facebookContent;
    if (facebookContent) {
      // console.log(this.state);
      facebookContent = null;
    } else {
      facebookContent = (
        <FacebookLogin
          appId="2176584609255929"
          // autoLoad
          fields="name,email,picture"
          callback={this.responseFacebook}
          onClick={this.componentClicked}
          render={renderProps => (
            <Button
              variant="contained"
              className={classes.facebookButton}
              size="small"
              onClick={renderProps.onClick}
            >
              <FacebookSVG />
              {/* <Typography color="inherit" className={classes.typography} /> */}
            </Button>
          )}
        />
      );
    }
    console.log(this.state);
    return <React.Fragment>{facebookContent}</React.Fragment>;
  }
}

export default withStyles(stylesLoginWithFacebook)(LoginWithFacebook);
