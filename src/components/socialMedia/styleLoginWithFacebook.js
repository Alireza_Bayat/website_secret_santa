import { fade } from "@material-ui/core/styles/colorManipulator";
const stylesLoginFacebook = theme => ({
  facebookButton: {
    color: "white",
    textTransform: "none",
    overflow: "hidden",
    textOverflow: "ellipsis",
    whiteSpace: "nowrap",
    maxHeight: "55px",
    minHeight: "55px",
    width: "auto",
    marginLeft: "20px",
    padding: theme.spacing.unit,
    align: "center",
    backgroundColor: fade("#3C5A99", 0.5),
    "&:hover": {
      backgroundColor: fade("#3C5A99", 1)
    }
  },

  typography: {
    // textTransform: "none",
    // align: "center"
    padding: "0.5em"
  }
});
export default stylesLoginFacebook;
