import React from "react";
import PropTypes from "prop-types";
import { NavLink } from "react-router-dom";
import Brand from "../appBarItems/Brand";
import Register from "../appBarItems/SignUp";

// Material ui core
import { withStyles } from "@material-ui/core/styles";
import classNames from "classnames";
import Toolbar from "@material-ui/core/Toolbar";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import Badge from "@material-ui/core/Badge";
import AppBar from "@material-ui/core/AppBar";
import Hidden from "@material-ui/core/Hidden";

// Material ui icon
import MenuIcon from "@material-ui/icons/Menu";
import NotificationsIcon from "@material-ui/icons/Notifications";

import Login from "../appBarItems/Login";

const drawerWidth = 240;

const styles = theme => ({
  root: {
    display: "flex"
  },
  toolbar: {
    paddingRight: 24 // keep right padding when drawer closed
  },
  button: {
    margin: theme.spacing.unit
  },
  toolbarIcon: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: "0 8px",
    ...theme.mixins.toolbar
  },
  appBar: {
    position: "absolute",
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    })
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  menuButton: {
    marginLeft: 12,
    marginRight: 36
  },

  menuButtonHidden: {
    display: "none",
    transform: `rotate(${360}deg)`,
    transition: "0.8s"
  },
  title: {
    flexGrow: 1
  }
  // To rotate burger button
  // burger: {
  //   transform: `rotate(${360}deg)`,
  //   transition: "0.8s"
  // }
});

const header = props => {
  const { classes } = props;
  return (
    <AppBar
      position="absolute"
      className={classNames(
        classes.appBar,
        props.drawerIsOpen && classes.appBarShift
      )}
    >
      <Toolbar disableGutters={!props.drawerIsOpen} className={classes.toolbar}>
        <IconButton
          color="inherit"
          aria-label="Open drawer"
          onClick={props.openDrawer}
          className={classNames(
            classes.menuButton,
            props.drawerIsOpen && classes.menuButtonHidden
          )}
        >
          <MenuIcon />
        </IconButton>
        <Hidden smDown>
          <Brand className={classes.title} />
        </Hidden>

        {/* <Hidden smDown> */}
        <Login />
        <Register />
        {/* </Hidden> */}
        <IconButton color="inherit">
          <Badge badgeContent={4} color="secondary">
            <NotificationsIcon />
          </Badge>
        </IconButton>
      </Toolbar>
    </AppBar>
  );
};

header.prototype = {
  classes: PropTypes.object.isRequired
};
export default withStyles(styles)(header);
