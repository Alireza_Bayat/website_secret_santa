import React from "react";
import classNames from "classnames";
import DrawerItems from "../list/DrawerItemList";
import PropTypes from "prop-types";

// Material ui core
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import IconButton from "@material-ui/core/IconButton";
import { withStyles } from "@material-ui/core/styles";
import Divider from "@material-ui/core/Divider";
import Typography from "@material-ui/core/Typography";

// Material ui Icons
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import DashboardIcon from "@material-ui/icons/DashboardOutlined";

const drawerWidth = 240;
const styles = theme => ({
  drawerPaper: {
    position: "relative",
    whiteSpace: "nowrap",
    width: drawerWidth,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  drawerPaperClose: {
    overflowX: "hidden",
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),
    width: theme.spacing.unit * 7,
    [theme.breakpoints.up("sm")]: {
      width: theme.spacing.unit * 9
    }
  },
  toolbarIcon: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: "0 8px",
    ...theme.mixins.toolbar
  }
});

const drawerPaper = props => {
  const { classes } = props;
  return (
    <Drawer
      variant="permanent"
      classes={{
        paper: classNames(
          classes.drawerPaper,
          !props.drawerIsOpen && classes.drawerPaperClose
        )
      }}
    >
      <div className={classes.toolbarIcon}>
        <DashboardIcon color="secondary" />
        <Typography
          className={classes.toolbarIcon}
          component="h1"
          variant="h6"
          color="secondary"
          noWrap
        >
          Santa Closet
        </Typography>
        <IconButton onClick={props.closeDrawer} color="inherit">
          <ChevronLeftIcon color="secondary" />
        </IconButton>
      </div>
      <Divider />
      <List>
        <DrawerItems />
      </List>
      <Divider />
    </Drawer>
  );
};

drawerPaper.prototype = {
  classes: PropTypes.object.isRequired
};
export default withStyles(styles)(drawerPaper);
