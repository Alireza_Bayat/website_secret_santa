import React, { Component } from "react";
import Header from "./Header";
import DrawerPaper from "./DrawerPaper";
import Main from "./Main";
import PropTypes from "prop-types";

// Material ui core
import { withStyles } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";

const styles = theme => ({
  root: {
    display: "flex"
  }
});

class Layout extends Component {
  state = {
    openDrawer: false
  };

  handleDrawerOpen = () => {
    this.setState({ openDrawer: true });
  };

  handleDrawerClose = () => {
    this.setState({ openDrawer: false });
  };

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <CssBaseline />
        <Header
          drawerIsOpen={this.state.openDrawer}
          openDrawer={this.handleDrawerOpen}
        />
        <DrawerPaper
          drawerIsOpen={this.state.openDrawer}
          closeDrawer={this.handleDrawerClose}
          id="0"
        />
        <Main />
      </div>
    );
  }
}
Layout.prototypes = {
  classes: PropTypes.object.isRequired
};
export default withStyles(styles)(Layout);
