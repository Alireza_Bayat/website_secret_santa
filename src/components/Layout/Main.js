import React from "react";
import { Switch, Route } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";

import AddGroupMember from "../group/AddGroupMember";
import AddNewGroup from "../group/AddNewGroup";
import Group from "../group/Group";
import Groups from "../group/Groups";
import MyGiftPreferences from "../gift/MyGiftPreferences";
import ShoppingCard from "../shopping/ShoppingCard";
import home from "../home/Home";

const styles = theme => ({
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
    height: "100vh",
    overflow: "auto"
  },
  chartContainer: {
    marginLeft: -22
  },
  tableContainer: {
    height: 320
  },
  h5: {
    marginBottom: theme.spacing.unit * 2
  }
});
const main = props => {
  return (
    <main className={props.classes.content}>
      <div className={props.classes.appBarSpacer} />
      <Switch>
        <Route path="/AddGroupMember" component={AddGroupMember} />
        <Route path="/AddNewGroup" component={AddNewGroup} />
        <Route path="/Group/:id" component={Group} />
        <Route path="/Groups" component={Groups} />
        <Route path="/Shopping" component={ShoppingCard} />
        <Route path="/MyGiftPreferences" component={MyGiftPreferences} />
        <Route path="/" component={home} />
      </Switch>
    </main>
  );
};
main.prototype = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(main);
