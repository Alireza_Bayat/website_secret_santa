import React, { Fragment } from "react";
import { connect } from "react-redux";
import * as actionTypes from "./store/actions/signUp";

import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import { withStyles } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import Paper from "@material-ui/core/Paper";

import Assignment from "@material-ui/icons/Assignment";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";

import Form from "../form/Form";
// import Eval from "../form/formValidation";
import withErrors from "../hoc/withErrors";
import { signUpStyles } from "./styles";

class Register extends Form {
  state = {
    open: false
  };
  schema = {
    name: () => {
      this.eVal
        .min(6)
        .max(14)
        .isRequire();
    },
    newEmail: () => {
      this.eVal
        .min(6)
        .isEmail()
        .isRequire();
    },
    password: () => {
      this.eVal
        .min(6)
        .max(10)
        .isRequire();
    },
    repeatPassword: () => {
      this.eVal.compare("password").isRequire();
    }
  };

  handleClickOpen = () => {
    this.setState({ open: true });
    // this.eVal = new Eval();
  };

  handleClose = () => {
    this.props.clearRegistrationForm();
    this.setState({ open: false, error: {} });
    delete this.state.errMessage;
    delete this.state.error;
  };
  updateStateHandler = name => event => {
    event.preventDefault();
    this.props.updateRegistrationForm(name, event.target.value);
  };

  handleRegistration = event => {
    event.preventDefault();
    const promiseValidation = new Promise((res, rej) => {
      res(this.onSubmissionValidate());
    });
    promiseValidation.then(() => {
      if (Object.keys(this.state.error).length === 0) {
        this.props.register(this.props.data);
        this.handleClose();
      }
    });
  };
  render() {
    const { classes } = this.props;
    console.log("errMessage", this.state.errMessage);
    return (
      <Fragment>
        <Button size="small" onClick={this.handleClickOpen}>
          Sign up
        </Button>
        <Dialog
          // fullWidth={false}
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
          disableBackdropClick
          maxWidth="xs"
        >
          <div className={classes.main}>
            <CssBaseline />
            <Button onClick={this.handleClose} color="secondary">
              X
            </Button>
            <Paper className={classes.paper}>
              <DialogActions />
              <Assignment
                color="primary"
                fontSize="large"
                className={classes.assignment}
              >
                <LockOutlinedIcon />
              </Assignment>
              <Typography component="h1" variant="h5">
                Sign up
              </Typography>
              <form className={classes.form} onSubmit={this.handleRegistration}>
                <Divider variant="middle" />

                {this.textField("name", "Name", "text", "name", "name", true)}

                <Typography variant="subtitle2" color="error" gutterBottom>
                  {this.state.errMessage ? this.state.errMessage.name : ""}
                </Typography>
                {this.textField("newEmail", "Email", "email", "email", "email")}

                <Typography variant="subtitle2" color="error" gutterBottom>
                  {this.state.errMessage ? this.state.errMessage.newEmail : ""}
                </Typography>
                {this.textField(
                  "password",
                  "Password",
                  "password",
                  "password",
                  "password"
                )}

                <Typography variant="subtitle2" color="error" gutterBottom>
                  {this.state.errMessage ? this.state.errMessage.password : ""}
                </Typography>
                {this.textField(
                  "repeatPassword",
                  "Repeat your password",
                  "password",
                  "repeatPassword",
                  "new-password"
                )}

                <Typography variant="subtitle2" color="error" gutterBottom>
                  {this.state.errMessage
                    ? this.state.errMessage.repeatPassword
                    : ""}
                </Typography>

                <DialogActions>
                  <Button
                    disabled={this.validateForm()}
                    color="primary"
                    type="submit"
                    className={classes.submit}
                    id="registerButton"
                    fullWidth
                    variant="contained"
                  >
                    Sign up
                  </Button>
                </DialogActions>
              </form>
            </Paper>
          </div>
        </Dialog>
      </Fragment>
    );
  }
}
const mapStateToProps = state => {
  return { data: state.signUp.registrationData };
};
const mapDispatchToProps = dispatch => {
  console.log(dispatch);
  return {
    register: data => dispatch(actionTypes.register(data)),
    updateRegistrationForm: (inputName, value) =>
      dispatch(actionTypes.updateRegistrationForm(inputName, value)),
    clearRegistrationForm: () => dispatch(actionTypes.clearRegistrationForm())
  };
};

const styledComponent = withStyles(signUpStyles)(Register);
export default withErrors(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(styledComponent)
);
