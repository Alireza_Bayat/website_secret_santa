import React from "react";
import { NavLink } from "react-router-dom";

// Material ui core
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/core/styles";

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
    flexGrow: 1
  },
  title: {
    flexGrow: 1
  }
});
const Brand = props => {
  return (
    <Button
      size="small"
      className={props.classes.button}
      component={NavLink}
      to="/"
      variant="text"
      //   color="secondary"
      id="1"
    >
      <Typography
        className={props.classes.title}
        component="h1"
        variant="h5"
        color="inherit"
        align="left"
        noWrap
      >
        Brand Name
      </Typography>
    </Button>
  );
};

export default withStyles(styles)(Brand);
