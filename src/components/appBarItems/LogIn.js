import React, { Fragment } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import * as actionTypes from "./store/actions/login";

import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Divider from "@material-ui/core/Divider";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import withStyles from "@material-ui/core/styles/withStyles";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";

import LockOutlinedIcon from "@material-ui/icons/LockOutlined";

import Form from "../form/Form";
import Eval from "../form/formValidation";
import withErrors from "../hoc/withErrors";
import { loginStyles } from "./styles";

class Login extends Form {
  state = {
    open: false
  };
  schema = {
    email: () => {
      this.eVal
        .min(6)
        .isEmail()
        .isRequire();
    },
    password: () => {
      this.eVal
        .min(6)
        .max(16)
        .isRequire();
    }
  };
  handleClickOpen = () => {
    this.setState({ open: true });
    this.eVal = new Eval();
  };
  handleClose = () => {
    this.props.clearLoginForm();
    this.setState({ open: false, error: {} });
    // delete this.eVal;
  };
  updateStateHandler = name => event => {
    event.preventDefault();
    this.props.update_LoginForm(name, event.target.value);
  };
  handleRegistration = event => {
    event.preventDefault();
    const promiseValidation = new Promise((res, rej) => {
      res(this.onSubmissionValidate());
    });
    promiseValidation.then(() => {
      this.props.onLogIn();
      this.handleClose();
    });
  };
  render() {
    const { classes } = this.props;
    return (
      <Fragment>
        <Button size="small" onClick={this.handleClickOpen}>
          Login
        </Button>
        <Dialog
          // fullWidth={true}
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
          disableBackdropClick
          maxWidth="xs"
        >
          <div className={classes.main}>
            <Button onClick={this.handleClose} color="secondary">
              X
            </Button>
            <CssBaseline />
            <Paper className={classes.paper}>
              <DialogActions />
              <Avatar className={classes.avatar}>
                <LockOutlinedIcon />
              </Avatar>
              <Typography component="h1" variant="h5">
                Login
              </Typography>
              <form className={classes.form} onSubmit={this.handleRegistration}>
                <Divider variant="middle" />
                {this.textField(
                  "email",
                  "Email Address",
                  "email",
                  "loginEmail",
                  "email",
                  true
                )}
                <Typography variant="subtitle2" color="error" gutterBottom>
                  {this.state.errMessage ? this.state.errMessage.email : ""}
                </Typography>
                {this.textField(
                  "password",
                  "Password",
                  "password",
                  "loginPassword",
                  "password"
                )}
                <Typography variant="subtitle2" color="error" gutterBottom>
                  {this.state.errMessage ? this.state.errMessage.password : ""}
                </Typography>
                <FormControlLabel
                  control={
                    <Checkbox
                      value="remember"
                      color="primary"
                      onClick={this.props.remember}
                      checked={this.props.rememberMe}
                    />
                  }
                  label="Remember me"
                />
                <DialogActions>
                  <Button
                    disabled={this.validateForm()}
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    className={classes.submit}
                    id="loginButton"
                  >
                    Login
                  </Button>
                </DialogActions>
                <Typography
                  className={classes.submit}
                  variant="subtitle2"
                  color="primary"
                  gutterBottom
                >
                  Forgot your password?
                </Typography>
              </form>
            </Paper>
          </div>
        </Dialog>
      </Fragment>
    );
  }
}

Login.propTypes = {
  classes: PropTypes.object.isRequired
};

const mapStateToProps = state => {
  return {
    data: state.login.user,
    loggedIn: state.login.loggedIn,
    rememberMe: state.login.rememberMe
  };
};
const mapDispatchToProps = dispatch => {
  return {
    update_LoginForm: (inputName, value) =>
      dispatch(actionTypes.update_LoginForm(inputName, value)),
    remember: () => dispatch(actionTypes.rememberMe()),
    onLogIn: () => dispatch(actionTypes.login()),
    clearLoginForm: () => dispatch(actionTypes.clearLoginForm())
  };
};

const styledComponent = withStyles(loginStyles)(Login);
export default withErrors(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(styledComponent)
);
