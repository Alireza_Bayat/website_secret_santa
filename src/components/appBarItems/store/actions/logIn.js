import * as actionTypes from "./actionTypes";

export const login = user => {
  return {
    type: actionTypes.LOGIN
    // email: user.email,
    // password: user.password
  };
};
//

export const update_LoginForm = (inputName, value) => {
  return {
    type: actionTypes.UPDATE_LOGIN_FORM,
    inputName: inputName,
    value: value
  };
};

export const rememberMe = () => {
  return {
    type: actionTypes.REMEMBER_ME
  };
};
export const clearLoginForm = () => {
  return {
    type: actionTypes.CLEAR_LOGIN_FORM
  };
};
// export const updateUser = (inputName, value) => {
//   return {
//     type: actionTypes.UPDATE_USER,
//     inputName: inputName,
//     value: value
//   };
// };
