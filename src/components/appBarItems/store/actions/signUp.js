import * as actionTypes from "./actionTypes";
export const register = data => {
  return {
    type: actionTypes.REGISTER
    // name: data.name,
    // email: data.email,
    // password: data.password,
    // repeatPassword: data.repeatPassword
  };
};

export const updateRegistrationForm = (inputName, value) => {
  return {
    type: actionTypes.UPDATE_REGISTRATION_FORM,
    inputName: inputName,
    value: value
  };
};

export const clearRegistrationForm = () => {
  return {
    type: actionTypes.CLEAR_REGISTRATION_FORM
    // name: "",
    // email: "",
    // password: "",
    // repeatPassword: ""
  };
};
