import * as actionTypes from "../actions/actionTypes";

const initialState = {
  registrationData: { name: "", newEmail: "", password: "", repeatPassword: "" }
};

const register = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.REGISTER:
      console.log("finalState", state.registrationData);
      return {
        state
        // ...state,
        // registrationData: {
        //   ...state.registrationData,
        //   name: action.name,
        //   newEmail: action.newEmail,
        //   password: action.password,
        //   repeatPassword: action.repeatPassword
        // }
      };

    case actionTypes.UPDATE_REGISTRATION_FORM:
      return {
        ...state,
        registrationData: {
          ...state.registrationData,
          [action.inputName]: action.value
        }
      };
    case actionTypes.CLEAR_REGISTRATION_FORM:
      return {
        ...state,
        registrationData: {
          ...state.registrationData,
          name: "",
          newEmail: "",
          password: "",
          repeatPassword: ""
        }
      };
    default:
      return state;
  }
};

export default register;
