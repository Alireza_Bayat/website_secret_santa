import * as actionTypes from "../actions/actionTypes";

const initialState = {
  user: { password: "", email: "" },
  loggedIn: false,
  rememberMe: false
};

const login = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.LOGIN:
      console.log("login clicked");
      return {
        ...state,
        loggedIn: true
      };
    case actionTypes.UPDATE_LOGIN_FORM:
      return {
        ...state,
        user: {
          ...state.user,
          [action.inputName]: action.value
        }
      };
    case actionTypes.REMEMBER_ME:
      return {
        ...state,
        rememberMe: !state.rememberMe
      };
    case actionTypes.CLEAR_LOGIN_FORM:
      return {
        ...state,
        user: { ...state.user, email: "", password: "" },
        loggedIn: false,
        rememberMe: false
      };

    default:
      return state;
  }
};

export default login;
