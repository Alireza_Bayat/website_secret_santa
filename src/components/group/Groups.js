import React, { Component } from "react";
import PropTypes from "prop-types";

import Typography from "@material-ui/core/Typography";
import Group from "./Group";

class Groups extends Component {
  state = {};
  render() {
    return (
      <Typography variant="h4" gutterBottom component="h2">
        Groups
      </Typography>
    );
  }
}
Group.prototypes = {
  classes: PropTypes.object.isRequired
};
export default Groups;
