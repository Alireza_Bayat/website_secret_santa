import React, { Component } from "react";
import PropTypes from "prop-types";

import Typography from "@material-ui/core/Typography";

class AddGroupMember extends Component {
  state = {};
  render() {
    return (
      <Typography variant="h4" gutterBottom component="h2">
        Add Group Member
      </Typography>
    );
  }
}
AddGroupMember.prototypes = {
  classes: PropTypes.object.isRequired
};
export default AddGroupMember;
