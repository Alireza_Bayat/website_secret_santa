import React, { Component } from "react";
import PropTypes from "prop-types";

import Typography from "@material-ui/core/Typography";

class Group extends Component {
  state = {};
  render() {
    return (
      <Typography variant="h4" gutterBottom component="h2">
        Group
      </Typography>
    );
  }
}

Group.prototypes = {
  classes: PropTypes.object.isRequired
};
export default Group;
