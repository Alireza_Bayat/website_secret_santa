import React from "react";
import PropTypes from "prop-types";
// Material ui core
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import ListItem from "@material-ui/core/ListItem";
import { withStyles } from "@material-ui/core/styles";

const styles = theme => ({
  menuItem: {
    "&:focus": {
      // backgroundColor: theme.palette.common.white,
      "& $primary, & $icon": {
        color: theme.palette.secondary.main
      }
    }
  },
  primary: { color: theme.palette.primary.light },
  icon: { color: theme.palette.primary.main }
});
const list = props => {
  const { classes } = props;
  return (
    <ListItem
      component={props.component}
      to={props.to}
      button
      id={props.id}
      className={classes.menuItem}
      value={props.value}
      onClick={props.click}
    >
      <ListItemIcon className={classes.icon}>{props.children}</ListItemIcon>
      <ListItemText
        classes={{ primary: classes.primary }}
        primary={props.text}
        inset
      />
    </ListItem>
  );
};

list.prototype = {
  classes: PropTypes.object.isRequired
};
export default withStyles(styles)(list);
