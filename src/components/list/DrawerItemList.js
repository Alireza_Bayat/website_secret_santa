import React, { Component } from "react";
import ListWithIcons from "./ListWithIcons";
import { NavLink } from "react-router-dom";
import PropTypes from "prop-types";
//
// Material ui core
import ListSubheader from "@material-ui/core/ListSubheader";
import Divider from "@material-ui/core/Divider";
//
// Material ui icons
import PeopleIcon from "@material-ui/icons/PeopleOutlined";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCartOutlined";
import GiftCardIcon from "@material-ui/icons/CardGiftcardOutlined";
import AddGroupMemberIcon from "@material-ui/icons/PersonAddOutlined";
import AddNewGroupIcon from "@material-ui/icons/GroupAddOutlined";
// import { registerDecorator } from "handlebars";

class DrawerItems extends Component {
  state = { selectedIcon: { id: "" }, value: 0 };

  handleSelection = event => {
    const selectedIcon = { ...this.state.selectedIcon };
    const id = event.currentTarget.children[0].children[0].id;
    selectedIcon.id = id;
    this.setState({ selectedIcon });
  };
  render() {
    const { id } = this.state.selectedIcon;
    return (
      <React.Fragment>
        <div>
          <ListWithIcons
            component={NavLink}
            to="/AddNewGroup"
            text="Add New Group"
            click={this.handleSelection}
          >
            <AddNewGroupIcon
              color={id === "1" ? "secondary" : "primary"}
              id="1"
            />
          </ListWithIcons>

          <ListWithIcons
            component={NavLink}
            to="/AddGroupMember"
            text="Add Group Member"
            click={this.handleSelection}
          >
            <AddGroupMemberIcon
              color={id === "2" ? "secondary" : "primary"}
              id="2"
            />
          </ListWithIcons>

          <ListWithIcons
            component={NavLink}
            to="/MyGiftPreferences"
            text="My Gift Preferences"
            click={this.handleSelection}
          >
            <GiftCardIcon color={id === "3" ? "secondary" : "primary"} id="3" />
          </ListWithIcons>

          <ListWithIcons
            component={NavLink}
            to="/Groups"
            text="Groups"
            click={this.handleSelection}
          >
            <PeopleIcon color={id === "4" ? "secondary" : "primary"} id="4" />
          </ListWithIcons>

          <ListWithIcons
            component={NavLink}
            to="/Shopping"
            text="Shopping"
            click={this.handleSelection}
          >
            <ShoppingCartIcon
              color={id === "5" ? "secondary" : "primary"}
              id="5"
            />
          </ListWithIcons>
        </div>
        <div>
          <Divider />
          <ListSubheader color="primary" inset>
            Recently Added Groups
          </ListSubheader>
          <ListWithIcons text="2018 Group 1">
            <PeopleIcon color="primary" />
          </ListWithIcons>
          <ListWithIcons text="2018 Group 2">
            <PeopleIcon color="primary" />
          </ListWithIcons>
          <ListWithIcons text="2017 Group 1">
            <PeopleIcon color="primary" />
          </ListWithIcons>
        </div>
      </React.Fragment>
    );
  }
}
DrawerItems.prototypes = {
  classes: PropTypes.object.isRequired
};
export default DrawerItems;
