import * as errorCodes from "./errorCodes";
import { errorMessage } from "./errorMessages";
class Eval {
  data = {};
  entity = "";
  entityValue = "";
  validityReport = {};
  constructor() {
    this.min = this.min.bind(this);
    this.max = this.max.bind(this);
    this.isRequire = this.isRequire.bind(this);
    this.compare = this.compare.bind(this);
  }

  isRequire = () => {
    if (this.entityValue.length > 0) {
      this.validityReport[this.entity][errorCodes.e0001] = true;
    } else {
      this.validityReport[this.entity][errorCodes.e0001] = false;
      this.validityReport[this.entity].errorMessage = errorMessage(
        errorCodes.e0001,
        ""
      );
    }
    return this;
  };
  min = min => {
    if (this.entityValue.length >= min) {
      this.validityReport[this.entity][errorCodes.e0002] = true;
    } else {
      this.validityReport[this.entity][errorCodes.e0002] = false;
      this.validityReport[this.entity].errorMessage = errorMessage(
        errorCodes.e0002,
        min
      );
    }
    return this;
  };
  max = max => {
    if (this.entityValue.length <= max) {
      this.validityReport[this.entity][errorCodes.e0003] = true;
    } else {
      this.validityReport[this.entity][errorCodes.e0003] = false;
      this.validityReport[this.entity].errorMessage = errorMessage(
        errorCodes.e0003,
        max
      );
    }
    return this;
  };
  isEmail = () => {
    const isEmail = /^[^@]+@[^@]+\.[^@]+$/.test(this.entityValue);
    if (isEmail) {
      this.validityReport[this.entity][errorCodes.e0004] = true;
    } else {
      this.validityReport[this.entity][errorCodes.e0004] = false;
      this.validityReport[this.entity].errorMessage = errorMessage(
        errorCodes.e0004,
        ""
      );
    }
    return this;
  };
  compare = string => {
    const isLocalCompare =
      this.data[string].localeCompare(this.entityValue) === 0;
    if (isLocalCompare) {
      this.validityReport[this.entity][errorCodes.e0005] = true;
    } else {
      this.validityReport[this.entity][errorCodes.e0005] = false;
      this.validityReport[this.entity].errorMessage = errorMessage(
        errorCodes.e0005,
        ""
      );
    }

    return this;
  };
  validate(schema, data) {
    console.log("evEM", this.eM);
    if (data) {
      this.data = data;
      const entities = Object.keys(data);
      entities.forEach(entity => {
        this.entityValue = data[entity];
        this.entity = entity;
        this.validityReport[entity] = {};
        schema[entity]();
      });
    }
  }
  hasError(validityReport) {}
}

export default Eval;
