import * as errorCodes from "./errorCodes";
export function errorMessage(errorCode, value) {
  let errorMessage = "";
  switch (errorCode) {
    case errorCodes.e0001:
      errorMessage = `Please complete this required field`;

      break;
    case errorCodes.e0002:
      errorMessage = `Minimum character allowed is ${value}`;

      break;
    case errorCodes.e0003:
      errorMessage = `Maximum input length allowed ${value}`;

      break;
    case errorCodes.e0004:
      errorMessage = `Email must be formated correctly `;

      break;
    case errorCodes.e0005:
      errorMessage = `Does not match with the password`;

      break;
    case errorCodes.e0006:
      errorMessage = `e0006 ${value}`;
      break;
    default:
      break;
  }
  return errorMessage;
}
