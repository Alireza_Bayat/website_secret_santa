import React, { Component } from "react";
import cloneDeep from "lodash/cloneDeep";

import InputBase from "@material-ui/core/InputBase";
import Tooltip from "@material-ui/core/Tooltip";
import Button from "@material-ui/core/Button";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Input from "@material-ui/core/Input";

import Eval from "../form/formValidation";

class Form extends Component {
  state = {
    error: {},
    errMessage: {},
    validityReport: {},
    allRequirementFulfilled: false
  };
  eVal = new Eval();
  handleChange = name => event => {
    const p = new Promise((res, rej) => {
      res(this.updateStateHandler(name)(event));
    });
    p.then(() => {
      console.log(this.schema);
      this.eVal.validate(this.schema, this.props.data);
      const validityReport = cloneDeep(this.eVal.validityReport);
      console.log("objVal", validityReport);
      this.getError(validityReport, name);
    });
  };
  getError = (validityReport, name) => {
    const error = { ...this.state.error };
    const errMessage = { ...this.state.errMessage };
    const errorValues = Object.values(validityReport[name]);
    const errorKeys = Object.keys(validityReport[name]);
    console.log("name", name);
    console.log("error", error);
    const allRequirementFulfilled = errorValues.every(eKey => {
      return eKey === true;
    });

    console.log("validityName", validityReport[name]);
    errorValues.forEach(eValue => {
      if (eValue === false) {
        error[name] = true;
        return;
      }
    });
    errorKeys.forEach(errorCode => {
      if (errorCode && !validityReport[name][errorCode]) {
        errMessage[name] = validityReport[name].errorMessage;
        return;
      }
    });
    if (allRequirementFulfilled) {
      console.log("delete name");
      delete error[name];
      delete errMessage[name];
    }
    this.setState({
      validityReport,
      error,
      allRequirementFulfilled,
      errMessage
    });
  };
  onSubmissionValidate = () => {
    Object.keys(this.state.validityReport).forEach(name => {
      this.getError(this.state.validityReport, name);
    });
  };
  validateForm = () => {
    console.log("validityReport", this.state.validityReport);
    return this.state.error && Object.values(this.state.error).length > 0
      ? true
      : false;
  };
  basicEmailInput = (classes, required = true, autoFocus = false) => {
    return (
      <Tooltip title={"Please enter the email you registered with us"}>
        <InputBase
          name="email"
          autoFocus={autoFocus}
          required={required}
          value={this.props.data.email}
          onChange={this.handleChange("email")}
          error={this.state.error.email}
          placeholder="Email"
          type="email"
          autoComplete="email"
          classes={{
            input: this.state.error.email ? classes.inputInput : classes.root,
            error: classes.inputError
          }}
        />
      </Tooltip>
    );
  };
  basicPasswordInput = (classes, required = true, autoFocus = false) => {
    return (
      <Tooltip title={"Please enter your password"}>
        <InputBase
          required={required}
          autoFocus={autoFocus}
          name="password"
          value={this.props.data.password}
          onChange={this.handleChange("password")}
          error={this.state.error.password}
          label="Password"
          placeholder="Password"
          type="password"
          autoComplete="password"
          classes={{
            // root: classes.inputRoot,
            input: this.state.error.password
              ? classes.inputInput
              : classes.root,
            error: classes.inputError
          }}
        />
      </Tooltip>
    );
  };
  textField = (
    name,
    label,
    type,
    id,
    autoComplete,
    autoFocus = false,
    required = true
  ) => {
    return (
      <FormControl
        margin="dense"
        required={required}
        fullWidth
        error={this.state.error && this.state.error[name]}
      >
        <InputLabel htmlFor={name}>{label}</InputLabel>
        <Input
          type={type}
          id={id}
          name={name}
          autoComplete={autoComplete}
          autoFocus={autoFocus}
          onChange={this.handleChange(name)}
          value={this.props.data ? this.props.data[name] : ""}
        />
      </FormControl>
    );
  };
  button = (
    name,
    classes,
    color = "inherit",
    size = "small",
    fullWidth = false,
    variant = "contained",
    id
  ) => {
    return (
      <Button
        size={size}
        disabled={this.validateForm()}
        color={color}
        type="submit"
        className={classes}
        id={id}
        fullWidth={fullWidth}
        variant={variant}
      >
        {name}
      </Button>
    );
  };
}

export default Form;
