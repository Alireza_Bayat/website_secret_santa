import React from "react";

const withErrors = WrappedComponent => {
  class WithError extends React.Component {
    state = {};

    render() {
      return <WrappedComponent />;
    }
  }

  return WithError;
};

export default withErrors;
