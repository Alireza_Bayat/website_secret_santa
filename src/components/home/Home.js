import React from "react";
import PropTypes from "prop-types";
import homeImage from "../../assets/images/Green_Christmas_Tree_Background.jpg";

import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";

const styles = {
  card: {
    // maxWidth: 345
    width: "100%"
  },
  media: {
    // ⚠️ object-fit is not supported by IE 11.
    objectFit: "cover"
  }
};
const home = props => {
  const { classes } = props;
  return (
    <Card className={classes.card}>
      <CardActionArea>
        <CardMedia
          component="img"
          alt="Green_Christmas_Tree"
          className={classes.media}
          // height="240"
          height="100%"
          image={homeImage}
          title="Christmas Tree"
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            Christmas
          </Typography>
          <Typography component="p">Green_Christmas_Tree</Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
        <Button size="small" color="primary">
          Share
        </Button>
        <Button size="small" color="primary">
          Learn More
        </Button>
      </CardActions>
    </Card>
  );
};

home.prototype = {
  classes: PropTypes.object.isRequired
};
export default withStyles(styles)(home);
