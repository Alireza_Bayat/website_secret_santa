var { DateTime } = require("luxon");
const clonedeep = require("lodash/cloneDeep");
const schema = {
  email: {
    isRequire: true,
    isEmail: true,
    min: 5,
    age: { isRequire: true, isNumber: true, min: 3, max: 9 }
  },
  age: { isRequire: true, isNumber: true, min: 3, max: 9 },
  password: { isRequire: true, isPassword: true, min: 9 }
};
// const name = { isRequire: true, min: 10 };
// schema.name = name;
// console.log(schema);
// console.log(Object.keys(schema));
const cloned = clonedeep(schema);

cloned.email.age.isNumber = false;

console.log("clone=>\n", cloned);
console.log("schema=>\n", schema);
// // const properties = ["email", "age", "password"];
// // const user = { email: "ab.com", age: "3", password: "1234" };
// // properties.forEach(property => {
// //   //   console.log(Object.keys(schema[property]));
// // });

// // const isNumber = (input, min, max) => {
// //   console.log(!Number.isNaN(input));
// //   if (!Number.isNaN(input)) {
// //     if (min && max) return max >= input && input >= min;
// //     else if (min) return input >= min;
// //     else if (max) return input <= max;
// //   } else {
// //     return false;
// //   }
// // };
// // const isRequire = input => {
// //   return input.length > 0;
// // };
// // const isDate = input => {
// //   return DateTime.fromISO(input).isValid;
// // };
// // const isString = (input, minLength, maxLength) => {
// //   if (typeof input === "string") {
// //     if (minLength && maxLength) {
// //       return minLength <= input.length <= maxLength;
// //     } else if (minLength) return input.length >= minLength;
// //     else if (maxLength) return input.length <= maxLength;
// //   }
// // };
// // const isEmail = input => {
// //   return /^[^@]+@[^@]+\.[^@]+$/.test(input);
// // };
// // const validationSwitchBoard = (schema, data) => {
// //   let isValid = false;
//   const keys = Object.keys(data);
//   keys.forEach(key => {
//     const conditions = Object.keys(schema[key]);
//     conditions.forEach(condition => {
//       switch (condition) {
//         case "isRequire":
//           isValid = isRequire(data[key]);
//           break;

//         case "isEmail":
//           isValid = isEmail(data[key]);
//           break;

//         case "isNumber":
//           isValid = isNumber(data[key], schema[key]["min"], schema[key]["max"]);
//           break;
//         case "isPassword":
//           isValid = isNumber(data[key], schema[key]["min"], schema[key]["max"]);
//           break;

//         default:
//           return isValid;
//       }
//     });
//   });
//   return isValid;
// };

// console.log(validationSwitchBoard(schema, user));

// const validateType = (type, elementValue, schema) => {
//   switch (type) {
//     case "email":
//       return /^[^@]+@[^@]+\.[^@]+$/.test(elementValue);
//     case "number":
//       return isNumber(elementValue, schema.min, schema.max);
//     case "date":
//       return DateTime.fromISO(elementValue).isValid;
//     case "string":
//       return elementValue === "string";
//     case "object":
//       return typeof elementValue === "object" && elementValue !== null;
//     default:
//       break;
//   }
// };
// Object.keys(schema["email"]).forEach(property => {
//   //   const conditionValue = schema["email"][property];
//   //   console.log(validationSwitchBoard(property, conditionValue, ""));
// });

// console.log(Number.isNaN("w" * 3));
// const d = "2019-11-12";
// // const d = "12/11/2019";
// const t = "fefef";
// const date = DateTime.fromISO(t);
// console.log(date);
// console.log(date.isValid);
// console.log(new Date(Date.parse(d)));
